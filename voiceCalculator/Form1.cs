﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Speech.Recognition;

enum MATH_FUNCTION
{
    NONE, SQUATEROOT
}

namespace voiceCalculator
{
    public partial class Form1 : System.Windows.Forms.Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        bool rus = false;
        bool en = true;

        static TextBox mainTextBox;

        static bool isSign = false;
        static char sign = ' ';

        static double first = 0.0;
        static double second = 0.0;

        static bool voice = false;

        static double result = 0;

        MATH_FUNCTION function = MATH_FUNCTION.NONE;

        private void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (voice)
            {
                if (e.Result.Confidence > 0.80)
                {
                    if (rus)
                    {
                        switch (e.Result.Text.ToString())
                        {
                            case "ноль":
                                updateTextInfo(0);
                                break;
                            case "один":
                                updateTextInfo(1);
                                break;
                            case "два":
                                updateTextInfo(2);
                                break;
                            case "три":
                                updateTextInfo(3);
                                break;
                            case "четыре":
                                updateTextInfo(4);
                                break;
                            case "пять":
                                updateTextInfo(5);
                                break;
                            case "шесть":
                                updateTextInfo(6);
                                break;
                            case "семь":
                                updateTextInfo(7);
                                break;
                            case "восемь":
                                updateTextInfo(8);
                                break;
                            case "девять":
                                updateTextInfo(9);
                                break;

                            case "плюс":
                                inputSign('+');
                                break;
                            case "минус":
                                inputSign('-');
                                break;
                            case "умножить":
                                inputSign('*');
                                break;
                            case "дилить":
                                inputSign('/');
                                break;

                            case "ровно":
                                result_func();
                                break;
                            case "очистить":
                                clear();
                                break;
                            case "стереть":
                                erase_func();
                                break;
                        }
                    }

                    if (en)
                    {
                        switch (e.Result.Text.ToString())
                        {
                            case "zero":
                                updateTextInfo(0);
                                break;
                            case "one":
                                updateTextInfo(1);
                                break;
                            case "two":
                                updateTextInfo(2);
                                break;
                            case "three":
                                updateTextInfo(3);
                                break;
                            case "four":
                                updateTextInfo(4);
                                break;
                            case "five":
                                updateTextInfo(5);
                                break;
                            case "six":
                                updateTextInfo(6);
                                break;
                            case "seven":
                                updateTextInfo(7);
                                break;
                            case "eight":
                                updateTextInfo(8);
                                break;
                            case "nine":
                                updateTextInfo(9);
                                break;

                            case "plus":
                                inputSign('+');
                                break;
                            case "minus":
                                inputSign('-');
                                break;
                            case "multiply":
                                inputSign('*');
                                break;
                            case "divide":
                                inputSign('/');
                                break;

                            case "result":
                                result_func();
                                break;
                            case "clear":
                                clear();
                                break;
                            case "erase":
                                erase_func();
                                break;
                        }
                    }
                }
            }
        }

        private bool InitializeSpeech()
        {
            string type = "";

            if (en)
                type = "en-us";
            else if (rus)
                type = "ru-ru";

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(type);
            SpeechRecognitionEngine sre = new SpeechRecognitionEngine(ci);

            try
            {
                sre.SetInputToDefaultAudioDevice();
            }
            catch
            {
                return false;
            }

            sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);


            Choices numbers = new Choices();

            if (rus)
                numbers.Add(new string[] { "нoль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "плюс", "минус", "умножить", "дилить", "ровно", "очистить", "стереть" });
            if (en)
                numbers.Add(new string[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "plus", "minus", "multiply", "divide", "result", "clear", "erase" });
  
            GrammarBuilder gb = new GrammarBuilder();
            gb.Culture = ci;
            gb.Append(numbers);


            Grammar g = new Grammar(gb);
            sre.LoadGrammar(g);

            sre.RecognizeAsync(RecognizeMode.Multiple);

            return true;
        }

        private void result_func()
        {
            if (isSign)
            {
                if (mainTextBox.Text != "" || mainTextBox.Text != sign.ToString())
                {
                    second = double.Parse(mainTextBox.Text);

                    switch (sign)
                    {
                        case '+':
                            result = first + second;
                            break;
                        case '-':
                            result = first - second;
                            break;
                        case '*':
                            result = first * second;
                            break;
                        case '/':
                            result = first / second;
                            break;
                        default:
                            result = 0;
                            break;
                    }

                    mainTextBox.Text = result.ToString();
                    isSign = false;
                }
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            mainTextBox = textBox1;
            if (!InitializeSpeech())
            {
                voiceToolStripMenuItem.Enabled = false;
            }
        }

        private void clear()
        {
            isSign = false;
            sign = ' ';
            first = 0;
            second = 0;

            mainTextBox.Clear();
        }

        private void erase_func()
        {
            if (mainTextBox.Text != "" && mainTextBox.Text != result.ToString())
            {
                if (mainTextBox.Text == sign.ToString())
                {
                    isSign = false;
                    sign = ' ';
                    mainTextBox.Text = first.ToString();
                }
                else
                    mainTextBox.Text = mainTextBox.Text.Remove(mainTextBox.TextLength - 1, 1);
            }
        }

        private void inputSign(char value)
        {
            mainTextBox.Font = new Font("Microsoft Sans Serif", 33);
            if (mainTextBox.Text != "" && isSign == false)
            {
                first = double.Parse(mainTextBox.Text);

                isSign = true;
                sign = value;
                mainTextBox.Text = value.ToString();
            }
        }

        private void updateTextInfo(double value)
        {
            if (mainTextBox.TextLength <= 18)
            {
                if (mainTextBox.Text == sign.ToString())
                    mainTextBox.Clear();
                mainTextBox.Text += value.ToString();
            }
            if (mainTextBox.TextLength == 8)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 30);
            if (mainTextBox.TextLength == 10)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 25);
            if (mainTextBox.TextLength == 12)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 20);
            if (mainTextBox.TextLength >= 15)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 15);
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            updateTextInfo(9);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            updateTextInfo(8);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            updateTextInfo(7);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            updateTextInfo(6);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            updateTextInfo(5);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            updateTextInfo(4);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            updateTextInfo(3);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            updateTextInfo(2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            updateTextInfo(1);
        }

        private void button0_Click(object sender, EventArgs e)
        {
            updateTextInfo(0);
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            inputSign('-');
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            inputSign('+');
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            inputSign('*');
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            inputSign('/');
        }

        private void buttonResult_Click(object sender, EventArgs e)
        {
            result_func();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            clear();
            mainTextBox.Font = new Font("Microsoft Sans Serif", 33);
        }

        private void buttonClearArrow_Click(object sender, EventArgs e)
        {
            erase_func();
            if (mainTextBox.TextLength == 7)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 33);
            if (mainTextBox.TextLength == 8)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 30);
            if (mainTextBox.TextLength == 10)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 25);
            if (mainTextBox.TextLength == 12)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 20);
            if (mainTextBox.TextLength == 15)
                mainTextBox.Font = new Font("Microsoft Sans Serif", 15);
        }

        private void onOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            voice = !voice;

            if (voice)
                onOffToolStripMenuItem.Text = "Off";
            else
                onOffToolStripMenuItem.Text = "On";
        }

        private void englishItem_Click(object sender, EventArgs e)
        {
            en = true;
            rus = false;
            laungeToolStripMenuItem.Image = englishItem.Image;
            InitializeSpeech();
        }

        private void rusItem_Click(object sender, EventArgs e)
        {
            rus = true;
            en = false;
            laungeToolStripMenuItem.Image = rusItem.Image;
            InitializeSpeech();
        }

        private void sqToolStripMenuItem_Click(object sender, EventArgs e)
        {
            calculateBox.Visible = true;
            function = MATH_FUNCTION.SQUATEROOT;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            switch(function)
            {
                case MATH_FUNCTION.SQUATEROOT:
                    double value = Int32.Parse(textBoxCalculateMenu.Text.ToString());
                    double result = Math.Sqrt(value);
                    textBoxCalculateMenu.Text = result.ToString();
                    break;
            }
        }

        private void buttonClearCalculateMenu_Click(object sender, EventArgs e)
        {
            textBoxCalculateMenu.Text = "";
        }

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            if (textBoxCalculateMenu.Text != "")
            {
                double value = double.Parse(textBoxCalculateMenu.Text.ToString());
                updateTextInfo(value);
                textBoxCalculateMenu.Text = "";
                calculateBox.Visible = false;
            }
            else
            {
                textBoxCalculateMenu.Text = "";
                calculateBox.Visible = false;
            }
        }
    }
}
